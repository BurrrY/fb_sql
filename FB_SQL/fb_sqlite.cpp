#include "fb_sqlite.h"

#include <QDebug>

#include <QFile>

FB_SQLite::FB_SQLite()
{

}

bool FB_SQLite::connnect(QString dbPath){
    qCInfo(fbsqlSQLite) << "call connnect(dbPath)";

    QFile dbFile(dbPath);
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setHostName("localhost");

    if(!dbFile.exists()) {
        qCCritical(fbsqlSQLite) << "DATABASE DOES NOT EXISTS!\nNEEDS TO BE CREATED.";
    }

    db.setDatabaseName(dbPath);
    FB_SQL::isOpen = db.open();
    if(!FB_SQL::isOpen) {
        lastError = db.lastError();

        qCCritical(fbsqlSQLite) << ("=====================");
        qCCritical(fbsqlSQLite) << (lastError.databaseText());
        qCCritical(fbsqlSQLite) << (lastError.driverText());
    }

    return FB_SQL::isOpen;
}


QStringList FB_SQLite::getTables()
{
    qCInfo(fbsqlSQLite) << "call getTables()";
    DB_DataSet ds = getData("SELECT tbl_name FROM sqlite_master  WHERE type = 'table'");
    QStringList tables;
    for(DB_DataRow row : ds) {
        tables.append(row["tbl_name"].toString());
    }

    return tables;
}


bool FB_SQLite::updateDB(QMap<QString,  QList<TableField>> tables)
{
    qCInfo(fbsqlSQLite) << "call updateDB(QMap tables)";

    QString query = "";
    QStringList existingTables = getTables();

    //Drop Tables
    for(QString tbl : existingTables)
    {
        if(!(tables.contains(tbl) || tbl == "sqlite_sequence")) {
            qCDebug(fbsqlSQLite) << "dropTable(" << tbl << ")";
            dropTable(tbl);
            existingTables.removeOne(tbl);
        }
    }

    //CreateTables
    QStringList tblList = tables.keys();
    for(QString tbl : tblList)
    {
        if(!existingTables.contains(tbl)) {
            qCDebug(fbsqlSQLite) << "Create table: " << tbl;
            createTable(tbl, tables.value(tbl));
            existingTables.append(tbl);
        }
    }

    //At this point, we have all required Tabels.
    //Now checking fields
    for(QString tbl : existingTables)
    {
        QMap<QString, TableField> crntFields = getTableFields(tbl);
        QList<QString> crntFieldNames = crntFields.keys();

        QMap<QString, TableField> trgtFields;
        QList<QString> trgtFieldNames;
        for(TableField fld : tables.value(tbl)) {
            trgtFieldNames << fld.Name;
            trgtFields.insert(fld.Name, fld);
        }


        for(QString fld : trgtFieldNames) {
            if(!crntFieldNames.contains(fld)) {
                addField(tbl, trgtFields.value(fld));
                qCDebug(fbsqlSQLite) << "Create Field " << fld << " in tbl: " << tbl;
            }
        }
    }



    qCDebug(fbsqlSQLite) << "Update done.";
}


bool FB_SQLite::addField(QString tableName, TableField field)
{
    qCInfo(fbsqlSQLite) << "call addField(" << tableName << ", field)";

    QString q = "ALTER TABLE `" + tableName + "` ADD COLUMN " + field.toString() + ";";
    return tryExecute(q);
}

bool FB_SQLite::dropField(QString tableName, TableField field, bool transaction)
{
    qCInfo(fbsqlSQLite) << "call dropField(" << tableName << ", field, transaction)";

}
