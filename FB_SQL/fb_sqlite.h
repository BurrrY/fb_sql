#ifndef FB_SQLITE_H
#define FB_SQLITE_H

#include "fb_sql.h"

class FB_SQLSHARED_EXPORT FB_SQLite : public FB_SQL
{
public:
    FB_SQLite();

    // FB_SQL interface
public:
    bool connnect(QString dbPath);
    QStringList getTables();
    bool updateDB(QMap<QString,  QList<TableField> > tables);
    bool addField(QString tableName, TableField field);
    bool dropField(QString tableName, TableField field, bool transaction = false);
};

#endif // FB_SQLITE_H
