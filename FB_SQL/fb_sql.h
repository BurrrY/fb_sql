#ifndef FB_SQL_H
#define FB_SQL_H


#include <QDateTime>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QVariant>

#include <QLoggingCategory>

Q_DECLARE_LOGGING_CATEGORY(fbsql)
Q_DECLARE_LOGGING_CATEGORY(fbsqlSQLite)



#include "fb_sql_global.h"





class FB_SQLSHARED_EXPORT TableField {
public:

    TableField() {}
    TableField(QString tblName, QString dataType, bool NOTNULL = false, bool isPK = false, bool AI = false, bool Unique = false, QString dfltValue = "")
    {
        Name = tblName;
        Type = dataType;
        notNull = NOTNULL;
        isPrimaryKey = isPK;
        autoincrement = AI;
        isUnique = Unique;
        defaultValue = dfltValue;
    }

    QString toString()
    {
        QString query = "`" + Name + "` " + Type + " ";

        if(notNull)
            query += "NOT NULL ";

        if(isPrimaryKey)
            query += "PRIMARY KEY ";

        if(autoincrement)
            query += "AUTOINCREMENT ";

        if(isUnique)
            query += "UNIQUE ";


        if(!defaultValue.isEmpty())
            query += "DEFAULT '" + defaultValue + "' ";

        return query;
    }

    bool operator==(const TableField &other) const {
        return (Name==other.Name)&&
                (Type==other.Type)&&
                (defaultValue==other.defaultValue)&&
                (notNull==other.notNull)&&
                (isPrimaryKey==other.isPrimaryKey)&&
                (autoincrement==other.autoincrement)&&
                (isUnique==other.isUnique);
      }

    QString Name;
    QString Type;
    QString defaultValue = "";
    bool notNull = false;
    bool isPrimaryKey = false;
    bool autoincrement = false;
    bool isUnique = false;
};



typedef QMap<QString, QVariant> DB_DataRow;
typedef QList<DB_DataRow> DB_DataSet;
typedef QList<TableField> DB_FieldList;
typedef QMap<QString,  DB_FieldList>  DB_TableDefinitons;




class Table {
public:
    Table(QString tblName, QList<TableField> tblFields)
    {
        Name = tblName;
        Fields = tblFields;
    }
    QString Name;
    QList<TableField> Fields;
};



class FB_SQLSHARED_EXPORT FB_SQL
{
public:
    FB_SQL();

    virtual bool connnect(QString dbPath) = 0;

    //DB-Structure Methods
    virtual bool updateDB(QMap<QString,  QList<TableField> > tables) = 0;
    virtual QStringList getTables() = 0;
    virtual bool addField(QString tableName, TableField field) = 0;
    virtual bool dropField(QString tableName, TableField field, bool transaction = false) = 0;

    bool createTable(QString tableName, QStringList fields);
    bool createTable(QString tableName, QList<TableField> fields);
    QMap<QString, TableField> getTableFields(QString TableName);
    bool dropTable(QString tblName);


    //Data-Handling
    DB_DataSet getData(QString query);
    DB_DataSet getData(QSqlQuery query);


    bool saveSetting(QString Key, QVariant Value);
    QVariant loadSetting(QString Key, QVariant fallbackValue = QString(""));

    static void setInstance(FB_SQL *value);
    static FB_SQL *getInstance();

    static QString SQL_DATEFORMAT(QDateTime d);
    static bool isOpen;

    bool tryExecute(QSqlQuery *q);
    bool tryExecute(QString q);

    QSqlDatabase* getDB();

protected:
    static FB_SQL* Instance;
    QSqlError lastError;
    QSqlDatabase db;
private:
    bool addSetting(QString Key, QVariant Value);
};

#endif // FB_SQL_H
