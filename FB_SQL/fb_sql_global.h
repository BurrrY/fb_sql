#ifndef FB_SQL_GLOBAL_H
#define FB_SQL_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(FB_SQL_LIBRARY)
#  define FB_SQLSHARED_EXPORT Q_DECL_EXPORT
#else
#  define FB_SQLSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // FB_SQL_GLOBAL_H
