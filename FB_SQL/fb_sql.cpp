#include "fb_sql.h"

#include <QFile>
#include <QFileInfo>

#include <QDebug>

Q_LOGGING_CATEGORY(fbsql, "fbsql")
Q_LOGGING_CATEGORY(fbsqlSQLite, "fbsql.SQLite")
bool FB_SQL::isOpen;



FB_SQL* FB_SQL::Instance = NULL;


FB_SQL::FB_SQL()
{
    qCInfo(fbsql) << "call FB_SQL()";
    FB_SQL::isOpen = false;

}

bool FB_SQL::createTable(QString tableName, QStringList fields)
{
    qCInfo(fbsql) << "call createTable()";
    throw;
}

bool FB_SQL::createTable(QString tableName, QList<TableField> fields)
{
    qCInfo(fbsql) << "call createTable()";
    if(tableName.length() <= 0)
        return false;

    if(fields.length() == 0)
        return false;

    QString query = "CREATE TABLE `" + tableName + "` (\n";
    for(TableField f : fields) {
        query += f.toString() + ", \n";
    }

    query = query.left(query.length() - 3) + "\n);";
    return tryExecute(query);
}

QMap<QString, TableField> FB_SQL::getTableFields(QString TableName)
{
    qCInfo(fbsql) << "call getTableFields()";
    QMap<QString, TableField> result;

    DB_DataSet d = getData("SELECT sql FROM sqlite_master WHERE tbl_name  = '" + TableName + "';");
    for(DB_DataRow r : d) {
        QString q = r["sql"].toString();
        q = q.mid(q.indexOf("(")+1, q.lastIndexOf(")") - q.indexOf("(")-1).replace("\n", "");
        QStringList rawFields = q.split(",");

        for(QString s : rawFields)
        {
            s = s.trimmed();
            QRegExp fieldName("['`\"]([a-zA-Z0-9_-]+)['`\"]\\s+(\\w+)");
            if (fieldName.indexIn(s) < 0 || fieldName.captureCount() != 2) {
                    qCWarning(fbsql) << "cannot get fieldname!" ;
            }
            else {
                TableField t(fieldName.cap(1).trimmed(), fieldName.cap(2).trimmed());

                t.notNull = s.contains("NOT NULL");
                t.autoincrement = s.contains("AUTOINCREMENT");
                t.isUnique = s.contains("UNIQUE");
                t.isPrimaryKey = s.contains("PRIMARY KEY");
                result.insert(t.Name, t);
            }
        }
    }

    return result;
}

bool FB_SQL::dropTable(QString tblName)
{
    qCInfo(fbsql) << "call dropTable()";
    return tryExecute("DROP TABLE `" + tblName + "`;");
}

DB_DataSet FB_SQL::getData(QString query) {
    qCInfo(fbsql) << "call getData(QString query)";
    QSqlQuery q(query);
    return getData(q);
}

DB_DataSet FB_SQL::getData(QSqlQuery q)
{
    qCInfo(fbsql) << "call getData(QSqlQuery q)";
    QList<QMap <QString, QVariant> >  result;

    tryExecute(&q);
    while(q.next())
    {
       QSqlRecord r = q.record();
       QMap<QString, QVariant> row;

       for(int i=0;i<r.count(); ++i)
           row.insert(r.fieldName(i), r.value(i));

       result.append(row);
    }

    return result;
}

bool FB_SQL::updateDB(QMap<QString,  QList<TableField>> tables)
{
    qCInfo(fbsql) << "call updateDB(QMap tables)";

}


FB_SQL *FB_SQL::getInstance()
{
    qCInfo(fbsql) << "call getInstance()";
    if(!FB_SQL::isOpen)
    {
        qCWarning(fbsql) << "Database is not open!";
    }

    return Instance;
}

void FB_SQL::setInstance(FB_SQL *value)
{
    qCInfo(fbsql) << "call setInstance()";
    Instance = value;
}

bool FB_SQL::saveSetting(QString Key, QVariant Value) {
    qCInfo(fbsql) << "call saveSetting(" << Key << ", " << Value << ")";

    QSqlQuery q;
    q.prepare("UPDATE settings SET Value = :Value WHERE Key = :Key;");
    q.bindValue(":Value", Value);
    q.bindValue(":Key", Key);
    bool saveOk = tryExecute(&q);

    int affectedRows = q.numRowsAffected();

    if(saveOk && affectedRows == 0 )
        return addSetting(Key, Value);
    else
        return saveOk;
}

QVariant FB_SQL::loadSetting(QString Key, QVariant fallbackValue)
{
    qCInfo(fbsql) << "call loadSetting(" << Key << ", " << fallbackValue << ")";

    QSqlQuery q;
    q.prepare("SELECT value FROM settings WHERE Key = :Key;");
    q.bindValue(":Key", Key);
    DB_DataSet ds = getData(q);
    if(ds.length()==1)
    {        
        DB_DataRow r = ds.at(0);
        qCDebug(fbsql) << "Load setting for Key: " << Key << " OK: " << r.value("Value") << ")";
        return r.value("Value");
    } else {
        qCDebug(fbsql) << "Load setting for Key: " << Key << " failed, return fallbackValue";
    }

    return fallbackValue;
}


bool FB_SQL::addSetting(QString Key, QVariant Value) {
    qCInfo(fbsql) << "call addSetting(" << Key << ", " << Value << ")";

    QSqlQuery q;
    q.prepare("INSERT INTO settings (Value, Key) VALUES(:Value, :Key);");
    q.bindValue(":Value", Value);
    q.bindValue(":Key", Key);
    return tryExecute(&q);
}

bool FB_SQL::tryExecute(QString q) {
    qCInfo(fbsql) << "call tryExecute(QString: \"" << q << "\")";

    QSqlQuery query;
    bool ok = query.exec(q);

    if(!ok) {
        QString queryStr = query.lastQuery();
        lastError = query.lastError();

        QString boundValues;
        QMapIterator<QString, QVariant> i(query.boundValues());
        while (i.hasNext()) {
            i.next();
            boundValues += i.key() + ": '" + i.value().toString() + "'; ";
        }

        qCCritical(fbsql) << ("=====================");
        qCCritical(fbsql) << (lastError.databaseText());
        qCCritical(fbsql) << (lastError.driverText());
        qCCritical(fbsql) << (queryStr);
        qCCritical(fbsql) << (boundValues);
    }

    return ok;
}

QSqlDatabase *FB_SQL::getDB()
{
    return &db;
}

bool FB_SQL::tryExecute(QSqlQuery *q) {
    qCInfo(fbsql) << "call tryExecute(QSqlQuery: q)";

    if(!db.isOpen())
        qCCritical(fbsql) << "DB not open!";

        bool queryOK = q->exec();
   // DBG_LV1(q->lastQuery());

    if(!queryOK) {
        QString queryStr = q->lastQuery();
        lastError = q->lastError();

        QString boundValues;
        QMapIterator<QString, QVariant> i(q->boundValues());
        while (i.hasNext()) {
            i.next();
            boundValues += i.key() + ": '" + i.value().toString() + "'; ";
        }

        qCCritical(fbsql) << ("=====================");
        qCCritical(fbsql) << (lastError.databaseText());
        qCCritical(fbsql) << (lastError.driverText());
        qCCritical(fbsql) << (queryStr);
        qCCritical(fbsql) << (boundValues);
    }

    return queryOK;

}


QString FB_SQL::SQL_DATEFORMAT(QDateTime d) {
    qCInfo(fbsql) << "call SQL_DATEFORMAT(QDateTime: \"" << d << "\")";
    return d.toString("yyyy-MM-dd hh:mm:ss");
}
