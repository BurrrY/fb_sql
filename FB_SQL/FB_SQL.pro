#-------------------------------------------------
#
# Project created by QtCreator 2016-03-24T20:29:52
#
#-------------------------------------------------

QT       += sql
QT       -= gui

TARGET = FB_SQL
TEMPLATE = lib
QMAKE_CXXFLAGS += -std=c++11


DEFINES += FB_SQL_LIBRARY

SOURCES += fb_sql.cpp \
    fb_sqlite.cpp

HEADERS += fb_sql.h\
        fb_sql_global.h \
    fb_sqlite.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
