#include <QString>
#include <QtTest>
#include <fb_sqlite.h>



class FB_SQL_TEST : public QObject
{
    Q_OBJECT

public:
    FB_SQL_TEST();
    FB_SQLite db;

private Q_SLOTS:
    void sqlite_DBInitAndStucture();
    void sqlite_DBUpdate();
};

FB_SQL_TEST::FB_SQL_TEST()
{
    QLoggingCategory::setFilterRules("*.debug=false\n"
                                         "fbsql.SQL.1.debug=true\n"
                                        "fbsql.SQL.2.debug=true\n"
                                        "fbsql.SQL.3.debug=false\n"
                                     "fbsql.SQLite.1.debug=true\n"
                                    "fbsql.SQLite.2.debug=true\n"
                                    "fbsql.SQLite.3.debug=false\n");
}

void FB_SQL_TEST::sqlite_DBUpdate() {


    QList<TableField> fields;

    fields << TableField("FieldA", "INTEGER", true, true);
    fields << TableField("FieldB", "TEXT");
    fields << TableField("FieldC", "TEXT", false, false, false, false, "NOPE");
    fields << TableField("FieldD", "INTEGER", false, false, false, false, "7");

    QMap<QString,  QList<TableField> > tblList_A;
    tblList_A.insert("Firsttable", fields);
    tblList_A.insert("FirsttableC", fields);

    db.updateDB(tblList_A);
    QCOMPARE(db.getTables().length(), 2);

    fields << TableField("FieldE", "INTEGER", false, false, false, false, "789");
    tblList_A.remove("FirsttableC");
    tblList_A.insert("FirsttableC", fields);
    db.updateDB(tblList_A);
}

void FB_SQL_TEST::sqlite_DBInitAndStucture()
{

    QString dbFileName = "test.db";
    //Cleanup
    QFile testDB(dbFileName);

    if(testDB.exists())
        QVERIFY(testDB.remove()); //Verify that we have a blank DB

    QVERIFY(!testDB.exists());


    QVERIFY(db.connnect(dbFileName));
    QCOMPARE(db.getTables().length(), 0);

    QList<TableField> fields;

    fields << TableField("FieldA", "INTEGER", true, true);
    fields << TableField("FieldB", "TEXT");
    fields << TableField("FieldC", "TEXT", false, false, false, false, "NOPE");
    fields << TableField("FieldD", "INTEGER", false, false, false, false, "7");

    QVERIFY(db.createTable("Firsttable", fields));
    QCOMPARE(db.getTables().length(), 1);
    QCOMPARE(db.getTables().at(0), QString("Firsttable"));


    QVERIFY(db.createTable("FirsttableB", fields));
    QCOMPARE(db.getTables().length(), 2);



    QMap<QString, TableField> existingFields = db.getTableFields("Firsttable");

    QCOMPARE(existingFields.count(), 4);

    QVERIFY(existingFields.contains("FieldA"));
    if(existingFields.contains("FieldA")) {
        QCOMPARE(existingFields.value("FieldA").Name, QString("FieldA"));
        QCOMPARE(existingFields.value("FieldA").Type, QString("INTEGER"));
        QVERIFY(existingFields.value("FieldA").isPrimaryKey);
        QVERIFY(existingFields.value("FieldA").notNull);
        QVERIFY(!existingFields.value("FieldA").autoincrement);
        QVERIFY(!existingFields.value("FieldA").isUnique);
    }

    QVERIFY(existingFields.contains("FieldB"));
    if(existingFields.contains("FieldB")) {
        QCOMPARE(existingFields.value("FieldB").Name, QString("FieldB"));
        QCOMPARE(existingFields.value("FieldB").Type, QString("TEXT"));
        QVERIFY(!existingFields.value("FieldB").isPrimaryKey);
        QVERIFY(!existingFields.value("FieldB").notNull);
        QVERIFY(!existingFields.value("FieldB").autoincrement);
        QVERIFY(!existingFields.value("FieldB").isUnique);
    }

}

QTEST_APPLESS_MAIN(FB_SQL_TEST)

#include "tst_fb_sql_test.moc"
