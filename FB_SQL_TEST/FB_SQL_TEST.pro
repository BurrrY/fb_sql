#-------------------------------------------------
#
# Project created by QtCreator 2016-03-24T20:45:07
#
#-------------------------------------------------

QT       += testlib sql

QT       -= gui

TARGET = tst_fb_sql_test
CONFIG   += console
CONFIG   -= app_bundle
QMAKE_CXXFLAGS += -std=c++11

TEMPLATE = app



SOURCES += tst_fb_sql_test.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

win32: LIBS += -L$$DESTDIR/ -lFB_SQL
else:unix: LIBS += -L$$DESTDIR/ -lFB_SQL

INCLUDEPATH += $$PWD/../FB_SQL
DEPENDPATH += $$PWD/../FB_SQL



